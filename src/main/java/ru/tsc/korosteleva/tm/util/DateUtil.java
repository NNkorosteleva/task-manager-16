package ru.tsc.korosteleva.tm.util;

import ru.tsc.korosteleva.tm.exception.field.DateIncorrectException;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    String PATTERN = "dd.MM.yyyy";

    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    static Date toDate(final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (Exception e) {
            throw new DateIncorrectException(value);
        }
    }

    static String toString(final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}
