package ru.tsc.korosteleva.tm.comparator;

import ru.tsc.korosteleva.tm.api.model.IHasCreated;

import java.util.Comparator;

public enum CreatedComparator implements Comparator<IHasCreated> {

    INSTANCE;

    @Override
    public int compare(final IHasCreated created1, final IHasCreated created2) {
        if (created1 == null || created2 == null) return 0;
        if (created1.getCreated() == null || created2.getCreated() == null) return 0;
        return created1.getCreated().compareTo(created2.getCreated());
    }

}
