package ru.tsc.korosteleva.tm.api.repository;

import ru.tsc.korosteleva.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
