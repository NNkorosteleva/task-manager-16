package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return projectRepository.create(name, description);
    }

    @Override
    public Project create(String name, String description, Date dateBegin, Date dateEnd) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null) throw new DateIncorrectException();
        if (dateEnd == null) throw new DateIncorrectException();
        return projectRepository.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return projectRepository.findAll(sort);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Project project = projectRepository.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index > projectRepository.getSize()) throw new IndexIncorrectException();
        Project project = projectRepository.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = projectRepository.findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = projectRepository.updateById(id, name, description);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index > projectRepository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = projectRepository.updateByIndex(index, name, description);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Project project = projectRepository.removeById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0 || index > projectRepository.getSize()) throw new IndexIncorrectException();
        Project project = projectRepository.removeByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = projectRepository.removeByName(name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Project project = projectRepository.changeProjectStatusById(id, status);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index > projectRepository.getSize()) throw new IndexIncorrectException();
        Project project = projectRepository.changeProjectStatusByIndex(index, status);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}